package com.training.frequenciesserver;

import java.util.Random;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class FrequenciesServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(FrequenciesServerApplication.class, args);
	}

	@GetMapping(value="/frequency")
	@ResponseBody
	public ResponseEntity<byte[]> generateFrequency(@RequestParam int size) {
		byte[] frequency = new byte[size];

		new Random().nextBytes(frequency);

		return ResponseEntity.ok().contentType(MediaType.APPLICATION_OCTET_STREAM).body(frequency);
	}
}
