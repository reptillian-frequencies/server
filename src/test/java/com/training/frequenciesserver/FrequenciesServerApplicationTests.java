package com.training.frequenciesserver;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class FrequenciesServerApplicationTests {

	private static final int RESPONSE_SIZE_IN_BYTES_32 = 32;
	private static final int RESPONSE_SIZE_IN_BYTES_64 = 64;

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	void whenGetToFrequencyEndpoint_shouldReturnResponseWithStatus200() throws Exception {
		ResponseEntity<byte[]> response = getResponse(RESPONSE_SIZE_IN_BYTES_32, byte[].class);

		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	void whenGetToFrequencyEndpoint_shouldReturnByteDataOfExpectedLength32Bytes() throws Exception {
		ResponseEntity<byte[]> response = getResponse(RESPONSE_SIZE_IN_BYTES_32, byte[].class);

		assertEquals(RESPONSE_SIZE_IN_BYTES_32, response.getBody().length);
	}

	@Test
	void whenGetToFrequencyEndpoint_shouldReturnByteDataOfExpectedLength64Bytes() throws Exception {
		ResponseEntity<byte[]> response = getResponse(RESPONSE_SIZE_IN_BYTES_64, byte[].class);

		assertEquals(RESPONSE_SIZE_IN_BYTES_64, response.getBody().length);
	}

	@Test
	void whenGettingTwoFrequencies_shouldReturnDifferentData() throws Exception {
		byte[] frequency1 = getResponse(RESPONSE_SIZE_IN_BYTES_32, byte[].class).getBody();
		byte[] frequency2 = getResponse(RESPONSE_SIZE_IN_BYTES_32, byte[].class).getBody();

		boolean isFrequeciesEqual = Arrays.equals(frequency1, frequency2);

		assertFalse(isFrequeciesEqual, "Got 2 equal frequencies: " + Arrays.toString(frequency1));
	}

	@Test
	void whenGetToFrequencyEndpoint_shouldReturnResponseWithContentTypeOctecStream() throws Exception {
		ResponseEntity<String> response = getResponse(RESPONSE_SIZE_IN_BYTES_32, String.class);

		MediaType expectedContentType = response.getHeaders().getContentType();

		assertEquals(MediaType.APPLICATION_OCTET_STREAM, expectedContentType);
	}

	private <T> ResponseEntity<T> getResponse(int responseSizeinBytes, Class<T> bodyClass) {
		return this.restTemplate.getForEntity(String.format("http://localhost:%d/frequency?size=%d",
				port , responseSizeinBytes), bodyClass);
	}

}
